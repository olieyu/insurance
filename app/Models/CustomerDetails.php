<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerDetails extends Model
{
    //Table Name
    protected $table = ' customer_details';
    //Primary Key
    public $primaryKey = 'id'; 
    //Timestamps
    public $timestamps = true;
    //use HasFactory;
}
