@extends('pages.index')

@section('content')

<div class="container">
    <h1>Products</h1>
    <div class="row row-cols-1 row-cols-md-3">
        @if(count($products) > 0)
            @foreach ($products as $product)
            <div class="col mb-4"> 
                <div class="card" style="width: 18rem;">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->title }}</h5>
                        <p class="card-text">{{ $product->content }}</p>
                        <p><small>written on {{$product->created_at}}</small></p>
                        <a href="#" class="btn btn-primary">Edit</a>
                    </div>
                 </div>
            </div>
            @endforeach
            
        @endif
    </div>
        <div class="row">
        {{$products->links()}}
        </div>
@endsection
