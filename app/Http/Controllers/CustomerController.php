<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer; 
use App\Models\CustomerDetails; 
use DB;


class CustomerController extends Controller
{
    public function index(){
        return view('pages.create');
    }
    public function store(Request $request)
    {  

        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|unique'
        ]);


        $data = $request->all();
        $result = Customer::insert($data);
        if($result){ 
        	$arr = array('msg' => 'Contact Added Successfully!', 'status' => true);
        }
        return Response()->json($arr);
        //return Response()->json($arr);
    }

    public function getCustomer($name, $email)
    {
        
        $details = DB::table('customer_details')
                ->select('customers.name', 'customers.email', 'customer_details.type_insurance', 
                'customer_details.premium_plan', 'customer_details.monthly_payment', )
                ->join('customers','customers.id', '=', 'customer_details.customerid')
                ->where('customers.name', '=', $name)
                ->where('customers.email', '=', $email)
                ->get();
        
        //$details = '2323';
       return ($details);
    }
}
