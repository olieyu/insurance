<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CustomerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [CustomerController::class, 'index']);
Route::post('jquery-ajax-form-submit', [CustomerController::class, 'store']);
Route::get('/{name}/{email}', [CustomerController::class, 'getCustomer']);
//Route::post('form-submit', 'CustomerController@store');

//Route::get('/', [ProductsController::class, 'index']);


//Route::get('/about', [PagesController::class, 'about']);
//Route::get('/view', [ProductsController::class, 'index']);
//Route::get('/create', [ProductsController::class, 'create']);


//Route::resource('product', [ProductsController::class]);

//Route::get('/test', 'PagesController@index');

