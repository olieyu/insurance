@extends('pages.index')

@section('content')

<div class="container">
    <h1>Products</h1>

    <form>
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="input_title" required>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="title">Content</label>
            <input type="text" class="form-control" id="input_content" required>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-group">
            <label for="title">Image</label>
            <input type="file" class="form-control-file" id="input_image" required>
            <small id="emailHelp" class="form-text text-muted"></small>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
@endsection
